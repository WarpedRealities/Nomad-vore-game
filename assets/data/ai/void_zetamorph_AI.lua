
function assault(controllable,sense,pos,seenHostile)
	if seenHostile:getRPG():hasStatus(167) then
		controllable:setAttack(2)
		controllable:Attack(hostile:getPosition().x,hostile:getPosition().y)
	else
		d=math.random(0,8)
		if (d>4) then
			controllable:setAttack(1)
			controllable:Attack(hostile:getPosition().x,hostile:getPosition().y)
		else
			controllable:setAttack(2)
			controllable:Attack(hostile:getPosition().x,hostile:getPosition().y)	
		end
	end

end

function attack(controllable,sense,pos,seenHostile) 
	if hostile:getRPG():hasStatus(166) then
		assault(controllable,sense,pos,seenHostile)
	else
		controllable:setAttack(0)
		controllable:Attack(seenHostile:getPosition().x,seenHostile:getPosition().y)
	end
end

function combat(controllable,sense,pos,hostile)

	if (controllable:getValue(0)==0) then
		controllable:setValue(0,1)	
		sense:drawText("You sense an insidious mental presence nearby")
	end

	seenHostile=sense:getHostile(controllable,10,true)
	if not (seenHostile == nil) then
		attack(controllable,sense,pos, seenHostile)
	else
		if controllable:HasPath() then
			controllable:FollowPath()
			controllable:addBusy(2)
		else
			controllable:Pathto(hostile:getPosition().x,hostile:getPosition().y,8)
		end		
	end 
	

end

function main(controllable, sense, script)  
	pos=controllable:getPosition()
	hostile=sense:getHostile(controllable,10,false)
	if not (hostile == nil ) and not controllable:getPeace() then
	--combat ai here
	combat(controllable,sense,pos,hostile)

	else
	a=math.random(0,8)
	controllable:move(a);
	end
end  