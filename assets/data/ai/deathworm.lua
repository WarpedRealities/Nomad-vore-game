function start(controllable)
	controllable:setValue(0,1)
	controllable:setPosition(99,99)
end

function combat(controllable,sense,script)
	pos=controllable:getPosition()
	hostile=sense:getHostile(controllable,10,true)
	if not (hostile == nil) then 
		if pos:getDistance(hostile:getPosition())<2 then
		--if in melee range attack
		controllable:Attack(hostile:getPosition().x,hostile:getPosition().y)
		else
		--if not move towards player
			if controllable:HasPath() then
			controllable:FollowPath()
			else
			controllable:Pathto(hostile:getPosition().x,hostile:getPosition().y,1)
			end
		end
	else
		start(controllable)
	end
end

function messageBehaviour(controllable, sense, script)
	messageState=controllable:getValue(4)
	alertness=controllable:getValue(1)
		if alertness <= 5 then
			messageState = 0
		end
		if alertness > 5 and alertness <= 10 then
		
			if messageState ~= 1 then
				
				sense:drawText("You see a dune shift nearby")		
			end
			messageState = 1
		end
		if alertness > 10 and alertness <= 15 then
			
			if  messageState ~= 2 then
		
				sense:drawText("You hear the sound of shifting sand close by")		
			end
			messageState = 2
		end
		if alertness > 15 and alertness <= 20 then

			if messageState ~= 3 then
				
				sense:drawText("You feel the vibration of something burrowing beneath you")		
			end
			messageState = 3
		end
	controllable:setValue(4,messageState)
end

function wormBehaviour(controllable,sense, script)
	alertness=controllable:getValue(1)
	
	x0=controllable:getValue(2)
	y0=controllable:getValue(3)	
	player=sense:getPlayer(controllable,false)
	x=player:getPosition().x
	y=player:getPosition().y
	if not (x==x0) or not (y==y0) then
		alertness = alertness+1		
		if alertness > 20 then
			controllable:setValue(0,2)
			sense:drawText("A deathworm erupts from beneath the sands!")
			controllable:setPosition(x0,y0)
		end
	else
		alertness = alertness-1
	end
	controllable:setValue(1,alertness)
	controllable:setValue(2,x)	
	controllable:setValue(3,y)

end

function main(controllable, sense, script)  
	a=controllable:getValue(0)
	if (a==0) then
		start(controllable)
	end
	if (a==2) then
		combat(controllable,sense,script)
	end
	if (a==1) then
		wormBehaviour(controllable,sense, script)
		messageBehaviour(controllable,sense,script)
		controllable:addBusy(4)
	end
end  
