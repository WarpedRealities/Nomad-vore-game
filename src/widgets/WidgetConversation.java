package widgets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.w3c.dom.Element;

import actor.player.Player;
import nomad.FlagField;
import shared.ParserHelper;
import view.ViewScene;

public class WidgetConversation extends WidgetBreakable {

	String conversationFileName;
	FlagField localFlags;
	boolean safeOnly;

	public WidgetConversation(Element node) {

		super(node);
		localFlags = new FlagField();
		safeOnly = !node.getAttribute("allowUnsafe").equals("true");

	}

	public void setConversationFileName(String conversationFileName) {
		this.conversationFileName = conversationFileName;
	}

	public void setSprite(int sprite) {
		widgetSpriteNumber = sprite;
	}

	public WidgetConversation(DataInputStream dstream) throws IOException {
		commonLoad(dstream);
		load(dstream);
		safeOnly = dstream.readBoolean();
		conversationFileName = ParserHelper.LoadString(dstream);
		localFlags = new FlagField();
		localFlags.load(dstream);
	}

	@Override
	public boolean safeOnly() {
		return safeOnly;
	}

	@Override
	public void save(DataOutputStream dstream) throws IOException {
		dstream.write(13);
		commonSave(dstream);
		super.saveBreakable(dstream);
		dstream.writeBoolean(safeOnly);
		ParserHelper.SaveString(dstream, conversationFileName);
		localFlags.save(dstream);
	}

	@Override
	public boolean Interact(Player player) {

		ViewScene.m_interface.StartConversation(conversationFileName, this);

		return false;
	}

	public FlagField getFlags() {
		return localFlags;
	}

}
