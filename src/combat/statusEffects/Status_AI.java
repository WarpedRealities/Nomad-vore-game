package combat.statusEffects;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import actor.npc.NPC;
import actorRPG.Actor_RPG;
import artificial_intelligence.BrainBank;
import artificial_intelligence.Script_AI;
import artificial_intelligence.detection.Sense;
import shared.ParserHelper;
import view.ViewScene;

public class Status_AI implements StatusEffect {
	int uid;
	int spriteIcon;
	int duration;
	String removeText, applyText;
	String behaviourScript;
	Script_AI controller;

	public Status_AI() {

	}

	public Status_AI(Element e) {
		uid = Integer.parseInt(e.getAttribute("uid"));
		spriteIcon = Integer.parseInt(e.getAttribute("icon"));
		if (e.getAttribute("duration").length() > 0) {
			duration = Integer.parseInt(e.getAttribute("duration"));
		}
		NodeList children = e.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element Enode = (Element) node;
				if (Enode.getTagName().equals("applyText")) {
					applyText = Enode.getTextContent();
				}
				if (Enode.getTagName().equals("removeText")) {
					removeText = Enode.getTextContent();
				}
			}
		}
		behaviourScript = e.getAttribute("script");
	}

	@Override
	public void load(DataInputStream dstream) throws IOException {
		uid = dstream.readInt();
		spriteIcon = dstream.readInt();
		duration = dstream.readInt();
		behaviourScript = ParserHelper.LoadString(dstream);
		removeText = ParserHelper.LoadString(dstream);
		applyText = ParserHelper.LoadString(dstream);
	}

	@Override
	public void save(DataOutputStream dstream) throws IOException {
		dstream.writeInt(9);
		dstream.writeInt(uid);
		dstream.writeInt(spriteIcon);
		dstream.writeInt(duration);
		ParserHelper.SaveString(dstream, behaviourScript);
		ParserHelper.SaveString(dstream, removeText);
		ParserHelper.SaveString(dstream, applyText);
	}

	@Override
	public void apply(Actor_RPG subject) {
		if (ViewScene.m_interface != null) {
			ViewScene.m_interface.DrawText(applyText.replace("TARGET", subject.getName()));
		}
	}

	@Override
	public void update(Actor_RPG subject) {

	}

	@Override
	public void remove(Actor_RPG subject, boolean messages) {
		if (ViewScene.m_interface != null && !messages) {
			ViewScene.m_interface.DrawText(removeText.replace("TARGET", subject.getName()));
		}

	}

	@Override
	public boolean maintain() {
		duration--;
		if (duration < 1) {
			return true;
		}
		return false;
	}

	@Override
	public StatusEffect cloneEffect() {
		Status_AI status = new Status_AI();
		status.behaviourScript = behaviourScript;
		status.duration = duration;
		status.removeText = removeText;
		status.applyText = applyText;
		status.spriteIcon = spriteIcon;
		status.uid = uid;
		return status;
	}

	@Override
	public int getStatusIcon() {
		return spriteIcon;
	}

	@Override
	public int getUID() {
		return uid;
	}

	public void runAI(NPC npc, Sense senseInterface) {
		if (controller == null) {
			controller = BrainBank.getInstance().getAI(behaviourScript);
		}
		controller.RunAI(npc, senseInterface);
	}

}
