package combat.statusEffects;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import actorRPG.RPG_Helper;
import item.instances.ItemDepletableInstance;
import nomad.universe.Universe;
import shared.ParserHelper;

public class Status_Energy_Defence extends Status_Defence {

	int energyCost = 0;
	private ItemDepletableInstance idi;

	public Status_Energy_Defence(Element e) {
		// TODO Auto-generated constructor stub
		uid = Integer.parseInt(e.getAttribute("uid"));
		spriteIcon = Integer.parseInt(e.getAttribute("icon"));
		if (e.getAttribute("duration").length() > 0) {
			duration = Integer.parseInt(e.getAttribute("duration"));
		}
		int count = Integer.parseInt(e.getAttribute("numModifiers"));
		modifiers = new AttribMod[count];
		NodeList children = e.getChildNodes();
		int index = 0;
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element Enode = (Element) node;
				if (Enode.getTagName() == "removeText") {
					removeText = Enode.getTextContent();
				}
				if (Enode.getTagName() == "effect") {
					modifiers[index] = new AttribMod();
					modifiers[index].attribute = RPG_Helper.AttributefromString(Enode.getAttribute("attribute"));
					modifiers[index].modifier = Integer.parseInt(Enode.getAttribute("modifier"));
					index++;
				}
				if (Enode.getTagName() == "strength") {
					strength = Integer.parseInt(Enode.getAttribute("value"));
				}
				if (Enode.getTagName().equals("cost")) {
					energyCost = Integer.parseInt(Enode.getAttribute("value"));
				}
			}
		}
	}

	@Override
	public boolean maintain() {

		if (duration < 1 || strength < 0) {
			return true;
		}
		return false;
	}

	@Override
	public StatusEffect cloneEffect() {
		Status_Energy_Defence status = new Status_Energy_Defence();
		status.energyCost = this.energyCost;
		status.duration = this.duration;
		status.removeText = this.removeText;
		status.spriteIcon = this.spriteIcon;
		status.uid = this.uid;
		status.strength = strength;
		status.modifiers = new AttribMod[modifiers.length];
		for (int i = 0; i < modifiers.length; i++) {
			status.modifiers[i] = modifiers[i];
		}
		return status;
	}

	public Status_Energy_Defence() {

	}
	@Override
	public int runDefence(int damage, int damageType) {

		boolean used = false;
		for (int i = 0; i < modifiers.length; i++) {
			if (modifiers[i].attribute == damageType) {

				damage -= modifiers[i].modifier;
				if (modifiers[i].modifier > 0) {
					used = true;
				}

			}
		}
		if (idi==null) {
			int s = this.getUID() - 5;
			if (ItemDepletableInstance.class
					.isInstance(Universe.getInstance().getPlayer().getInventory().getSlot(s % 10))) {
				idi = (ItemDepletableInstance) Universe.getInstance().getPlayer().getInventory().getSlot(s % 10);
			}
		}
		if (idi != null && used) {
			idi.setEnergy(idi.getEnergy() - energyCost);
			if (idi.getEnergy()<=0) {
				duration = 0;
			}
			else
			{
				duration = 100;
			}
		}

		return damage;
	}

	@Override
	public void load(DataInputStream dstream) throws IOException {
		uid = dstream.readInt();
		energyCost = dstream.readInt();
		spriteIcon = dstream.readInt();
		duration = dstream.readInt();
		strength = dstream.readInt();
		int c = dstream.readInt();
		modifiers = new AttribMod[c];
		for (int i = 0; i < c; i++) {
			modifiers[i] = new AttribMod();
			modifiers[i].load(dstream);
		}
		removeText = ParserHelper.LoadString(dstream);
	}

	@Override
	public void save(DataOutputStream dstream) throws IOException {
		dstream.writeInt(11);
		dstream.writeInt(uid);
		dstream.writeInt(energyCost);
		dstream.writeInt(spriteIcon);
		dstream.writeInt(duration);
		dstream.writeInt(strength);
		dstream.writeInt(modifiers.length);
		for (int i = 0; i < modifiers.length; i++) {
			modifiers[i].save(dstream);
		}
		ParserHelper.SaveString(dstream, removeText);

	}

	public int getEnergyCost() {
		return energyCost;
	}

}
