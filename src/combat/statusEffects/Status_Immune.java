package combat.statusEffects;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.w3c.dom.Element;

import actorRPG.Actor_RPG;
import shared.ParserHelper;
import view.ViewScene;

public class Status_Immune implements StatusEffect {

	int uid;
	int spriteIcon;
	int duration;
	String tag;
	String removeText;

	public Status_Immune() {

	}

	public Status_Immune(Element e) {
		uid = Integer.parseInt(e.getAttribute("uid"));
		spriteIcon = Integer.parseInt(e.getAttribute("icon"));

		if (e.getAttribute("duration").length() > 0) {
			duration = Integer.parseInt(e.getAttribute("duration"));
		}
		tag = e.getAttribute("tag");
		removeText = e.getTextContent();
	}

	@Override
	public void load(DataInputStream dstream) throws IOException {
		uid = dstream.readInt();
		spriteIcon = dstream.readInt();
		duration = dstream.readInt();
		removeText = ParserHelper.LoadString(dstream);
		tag = ParserHelper.LoadString(dstream);
	}

	@Override
	public void save(DataOutputStream dstream) throws IOException {
		dstream.writeInt(10);
		dstream.writeInt(uid);
		dstream.writeInt(spriteIcon);
		dstream.writeInt(duration);
		ParserHelper.SaveString(dstream, removeText);
		ParserHelper.SaveString(dstream, tag);
	}

	@Override
	public void apply(Actor_RPG subject) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Actor_RPG subject) {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Actor_RPG subject, boolean messages) {
		if (ViewScene.m_interface != null && messages) {
			ViewScene.m_interface.DrawText(removeText.replace("TARGET", subject.getName()));
		}
	}

	@Override
	public boolean maintain() {
		duration--;
		if (duration < 1) {
			return true;
		}
		return false;
	}

	@Override
	public StatusEffect cloneEffect() {
		Status_Immune status = new Status_Immune();
		status.removeText = this.removeText;
		status.duration = this.duration;
		status.spriteIcon = this.spriteIcon;
		status.removeText = this.removeText;
		status.tag = this.tag;
		return status;
	}

	public boolean containsTag(String t) {
		return tag.contains(t);
	}

	@Override
	public int getStatusIcon() {

		return spriteIcon;
	}

	@Override
	public int getUID() {

		return uid;
	}

}
